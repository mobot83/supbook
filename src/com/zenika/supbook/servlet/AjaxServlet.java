package com.zenika.supbook.servlet;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zenika.supbook.mail.SendMail;
import com.zenika.supbook.model.User;
import com.zenika.supbook.service.UserService;

public class AjaxServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1184148845230583244L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String username = request.getParameter("username").trim();
		if(username == null || "".equals(username.trim())){
			return;
		}
		
		UserService service = new UserService();
		List<User> user = service.searchFriendByName(12L, username);
		Gson gson = new Gson();
		String json = gson.toJson(user);
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String address = request.getParameter("invit_mail");
		if(address != null){
			SendMail mail = new SendMail();
			try {
				mail.sendMail();
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		response.sendRedirect(request.getContextPath() + "/public/");
	}
}
