package com.zenika.supbook.model;

public class Message extends AbstractBusinessObject{
	
	private String content;
	private Long senderId;
	private Long receiverId;
	private Boolean isPrivate;
	
	public Message(){}
	
	public Message(String content, Long senderId) {
		super();
		this.content = content;
		this.senderId = senderId;
		this.isPrivate = true;
	}
	
	public Message(String content, Long senderId, Long receiverId) {
		super();
		this.content = content;
		this.senderId = senderId;
		this.receiverId = receiverId;
	}
	
	public Message(String content, Long senderId, Long receiverId, Boolean isPrivate) {
		super();
		this.content = content;
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.isPrivate = isPrivate;
	}
	
	public Message(Long id, String content, Long senderId, Long receiverId) {
		super();
		this.setId(id);
		this.content = content;
		this.senderId = senderId;
		this.receiverId = receiverId;
	}
	
	public Message(Long id, String content, Long senderId, Long receiverId, Boolean isPrivate) {
		super();
		this.setId(id);
		this.content = content;
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.isPrivate = isPrivate;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Long getSenderId() {
		return senderId;
	}
	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public Long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Long receiverId) {
		this.receiverId = receiverId;
	}

	public Boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	

}
