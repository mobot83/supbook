package com.zenika.supbook.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.zenika.supbook.model.Message;

public class MessageService {

	public Message create(Message message){
		Connection cx = ConnexionManager.getCurrentConnection();
		PreparedStatement ps = null;
		try {

			String sql = "INSERT INTO message ( sender_id, receiver_id, content, is_private ) VALUES ( ?, ?, ?, ? ) ";

			ps = cx.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setLong(1, message.getSenderId());
			ps.setLong(2, message.getReceiverId());
			ps.setString(3, message.getContent());
			ps.setBoolean(4, message.getIsPrivate());
			ps.executeUpdate();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				message.setId(generatedKeys.getLong(1));
			}
			else {
				throw new SQLException("Creating message failed, no ID obtained.");
			}

			cx.commit();
			return message;

		} catch (Exception e) {
			throw new RuntimeException(e);

		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	public boolean update(Message message) {
		Connection cx = ConnexionManager.getCurrentConnection();
		PreparedStatement ps = null;
		try {

			ps = cx.prepareStatement("UPDATE message SET content = ? WHERE ID = ? ");
			ps.setString(1, message.getContent());
			ps.setLong(2, message.getId());
			ps.executeUpdate();
			boolean result = (ps.getUpdateCount() == 1);
			cx.commit();

			return result;

		} catch (Exception e) {
			throw new RuntimeException(e);

		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	public List<Message> findAll() {
		List<Message> result = new ArrayList<Message>();

		Connection cx = ConnexionManager.getCurrentConnection();

		PreparedStatement ps = null;
		try {
			ps = cx.prepareStatement("SELECT id, sender_id, receiver_id, content, is_private FROM message");

			ps.execute();
			ResultSet rs = ps.getResultSet();
			while (rs.next()) {
				result.add(buildFromResultSet(rs));
			}

			return result;

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	public List<Message> getNews(Long userId) {
		List<Message> result = new ArrayList<Message>();

		Connection cx = ConnexionManager.getCurrentConnection();

		PreparedStatement ps = null;
		try {
			ps = cx.prepareStatement("SELECT id, sender_id, receiver_id, content, is_private FROM message "
					+ "JOIN users_friends ON(sender_id = friend_id) "
					+ "WHERE user_id = ? AND receiver_id = ?");

			ps.setLong(1, userId);
			ps.setLong(2, userId);
			ps.execute();
			ResultSet rs = ps.getResultSet();
			while (rs.next()) {
				result.add(buildFromResultSet(rs));
			}

			return result;

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	public Message readById(long id) {
		Connection cx = ConnexionManager.getCurrentConnection();

		PreparedStatement ps = null;
		try {
			ps = cx.prepareStatement("SELECT id, sender_id, receiver_id, content, is_private FROM message WHERE ID = ?");
			ps.setLong(1, id);
			ps.execute();
			ResultSet rs = ps.getResultSet();
			Message result = null;
			if (rs.next()) {
				result = buildFromResultSet(rs);
			}

			return result;

		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	private Message buildFromResultSet(ResultSet rs) throws SQLException {
		return 
			new Message(rs.getLong("id"), rs.getString("content"), rs.getLong("sender_id"), rs.getLong("receiver_id"), rs.getBoolean("is_private"));
	}
	
}
