package com.zenika.supbook.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.zenika.supbook.model.User;

public class UserService {
	
	public User create(User user) {
		Connection cx = ConnexionManager.getCurrentConnection();
		PreparedStatement ps = null;
		try {

			String sql = "INSERT INTO users ( LOGIN, EMAIL ) VALUES ( ?, ? ) ";

			ps = cx.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, user.getLogin());
			ps.setString(2, user.getEmail());
			ps.executeUpdate();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				user.setId(generatedKeys.getLong(1));
			}
			else {
				throw new SQLException("Creating user failed, no ID obtained.");
			}

			cx.commit();
			return user;

		} catch (Exception e) {
			throw new RuntimeException(e);

		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	public boolean update(User user) {
		Connection cx = ConnexionManager.getCurrentConnection();
		PreparedStatement ps = null;
		try {

			ps = cx.prepareStatement("UPDATE USERS SET LOGIN = ?, EMAIL = ? WHERE ID = ? ");
			ps.setString(1, user.getLogin());
			ps.setString(2, user.getEmail());
			ps.setLong(3, user.getId());
			ps.executeUpdate();
			boolean result = (ps.getUpdateCount() == 1);
			cx.commit();

			return result;

		} catch (Exception e) {
			throw new RuntimeException(e);

		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}

	public List<User> findAll() {
		List<User> result = new ArrayList<User>();

		Connection cx = ConnexionManager.getCurrentConnection();

		PreparedStatement ps = null;
		try {
			ps = cx.prepareStatement("SELECT ID, LOGIN, EMAIL FROM USERS");

			ps.execute();
			ResultSet rs = ps.getResultSet();
			while (rs.next()) {
				result.add(buildFromResultSet(rs));
			}

			return result;

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}

	public User readById(long id) {
		Connection cx = ConnexionManager.getCurrentConnection();

		PreparedStatement ps = null;
		try {
			ps = cx.prepareStatement("SELECT ID, LOGIN, EMAIL FROM USERS WHERE ID = ?");
			ps.setLong(1, id);
			ps.execute();
			ResultSet rs = ps.getResultSet();
			User result = null;
			if (rs.next()) {
				result = buildFromResultSet(rs);
			}

			return result;

		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	public List<User> searchFriendByName(Long userId, String username){
		Connection cx = ConnexionManager.getCurrentConnection();
		PreparedStatement ps = null;
		try {
			ps = cx.prepareStatement(
					"SELECT id, login, email FROM users JOIN users_friends ON (friend_id = id)" +					
							"where user_id = ? and login LIKE ?"
			);
			ps.setLong(1, userId);
			ps.setString(2, username + "%");
			ps.execute();
			ResultSet rs = ps.getResultSet();
			List<User> result = new ArrayList<User>();
			while (rs.next()) {
				result.add(buildFromResultSet(rs));
			}

			return result;

		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	public boolean delete(long id) {
		Connection cx = ConnexionManager.getCurrentConnection();

		PreparedStatement ps = null;
		try {
			ps = cx.prepareStatement("DELETE FROM USERS WHERE ID = ?");
			ps.setLong(1, id);
			ps.executeUpdate();
			boolean result = (ps.getUpdateCount() == 1);
			
			cx.commit();

			return result;

		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			ConnexionManager.closeProperly(ps, cx);
		}
	}
	
	private User buildFromResultSet(ResultSet rs) throws SQLException {
		User result = new User();
		result.setId(rs.getLong("ID"));
		result.setLogin(rs.getString("LOGIN"));
		result.setEmail(rs.getString("EMAIL"));
		return result;
	}
}
