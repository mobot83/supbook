<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<title>Invitation</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/style/mail.css" />
</head>

<body>
    <div class="invitation">
    <h1>Invitation</h1>
    <br>
    <p>Ce formulaire vous permettra d'envoyer une invitation au r�seau SupBook.</p>
    <br><br>
    <form action="<%=request.getContextPath()%>/AjaxServlet"
		method="POST">
	    <label>Entrer une adresse mail:</label>
	    <input type="text" name="invit_mail" id="invit_mail" placeholder="CampusID@supinfo.com"><br>
	    <br><br>
	    <input type="submit" value="Envoyer">
    </form>
    </div>
</body>
</html>