<%@page import="com.zenika.supbook.model.Message"%>
<%@page import="com.zenika.supbook.service.MessageService"%>
<%
	String content = request.getParameter("content");
	String user = request.getParameter("secret");
	Long userID = Long.valueOf(user);
	String strIsPrivate = request.getParameter("isPrivate");
	
	if (content != null) {
		Boolean isPrivate = false;
		if(strIsPrivate != null){
			isPrivate = true;
		}
		Message message = new Message(content, 12L, userID, isPrivate);
		MessageService service = new MessageService();
		service.create(message);
		response.sendRedirect(request.getContextPath() + "/public/sendMessageForm.jsp");
		
	} else {
		response.sendRedirect(request.getContextPath() + "/public/");
		
		
	}
%>