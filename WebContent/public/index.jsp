<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@page import="com.zenika.supbook.model.User"%>
<%@page import="com.zenika.supbook.service.UserService"%>
<%@page import="com.zenika.supbook.model.Message"%>
<%@page import="com.zenika.supbook.service.MessageService"%>
<%
MessageService service = new MessageService();
List<Message> list = service.getNews(12L);
List<User> users = new ArrayList<User>();

for(int i=0; i<list.size();i++){
    	UserService userService = new UserService();
    	users.add(userService.readById(((Message)list.get(i)).getSenderId()));
}
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<title>Fil d'actu</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/style/fil_actu.css" />
<script src="http://code.jquery.com/jquery-1.10.2.js"
				type="text/javascript"></script>
</head>

<body>
    <div class="fil_actu">
    <h1>Fil d'actu</h1>
    <div class="search">
    <input type="search" id="search" placeholder="Rechechez">
    <input type="image" src="<%=request.getContextPath()%>/images/loupe_icon.png" id="loupe">
    </div>
    <ul id="user-list">
    </ul>
    <div class="message_actu">
    	<%
    	for(int i=0; i<list.size();i++){%>
	        <label> Envoy� par: 
	        <%= users.get(i).getLogin() %></label>
	        <div class="message_content">
	        	Message:
	        	<p><%= ((Message)list.get(i)).getContent() %></p>
	        </div>
      	<%}%>
    </div>
    </div>
    <script>
    $('#loupe').click(function(event) {� 
�����������var username=$('#search').val();
   	�����������$.get('<%=request.getContextPath()%>/AjaxServlet',{username:username},function(data) {
  					$.each(data, function(index) {
  						$('#user-list').append(
  							$('<li>').append(
  							        $('<a>').attr('class','user-select').attr('id', data[index].id).append(
  							            $('<span>').attr('class', 'tab').append(data[index].login)
  						)));
  					});������ 
  	������������});
  	���������});
    </script>
</body>
</html>