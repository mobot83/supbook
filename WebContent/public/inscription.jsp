<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Inscription</title>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/style/inscription.css" />
    </head>

    <body>

    <div class="inscription">
        <h1>Inscription</h1>

    <div class="prenom">
        <label>Entrez votre pr�nom:</label>
        <input type="text" name="prenom" id="prenom" placeholder="Mon Pr�nom">
    </div>
    

    <div class="nom">
        <label>Entrez votre nom:</label>
        <input type="text" name="nom" id="nom" placeholder="Mon nom de Famille">
    </div>
    

    <div class="birth">
        <label>Entrez votre date de naissance:</label>
        <input id="date" type="date">
    </div>
    

    <div class="upload">
        <form method="post" action="reception-photo" enctype="multipart/form-data">
            <label for="mon_fichier">Importez une photo (max. 5 Mo):</label>
            <input type="hidden" name="MAX_FILE_SIZE" value="5242880" />
            <input type="file" name="mon_fichier" id="mon_fichier" />
        </form>
    </div>
    <div class="valider">
        <input type="submit" id="valider" value="Valider">
    </div>
    </div>
    </body>
</html>